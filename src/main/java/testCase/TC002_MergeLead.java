package testCase;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import WdMethods.ProjectMethods;
import WdMethods.SeMethods;

public class TC002_MergeLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		Testcasename = "TC003";
		TestCasedescription ="MergeLead";
		Category = "Sanity";
		Author= "vicky";
	}
	@Test
	public void mergelead() throws InterruptedException {

		login();
	WebElement ele5 = locateElement("LinkText", "Leads");
	click(ele5);
	
	WebElement ele6 = locateElement("LinkText", "Merge Leads");
	click(ele6);
	
	WebElement ele7 = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
	click(ele7);
	
	switchToWindow(1);
	
	WebElement ele8 = locateElement("name", "firstName");	
	type(ele8, "vignesh");
	
	WebElement ele9 = locateElement("xpath", "//button[text()='Find Leads']");
	click(ele9);
	
	/*WebElement ele10 = locateElement("xpath", "//a[text()='VIGNESH']");
	click(ele10);*/
	driver.findElementByXPath("//a[text()='vignesh1']").click();
	
	switchToWindow(0);
	WebElement ele11 = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
	click(ele11);
	
	switchToWindow(1);
	

	WebElement ele12 = locateElement("name", "firstName");	
	type(ele12, "vicky");
	
	WebElement ele13 = locateElement("xpath", "//button[text()='Find Leads']");
	click(ele13);
	
/*	WebElement ele14 = locateElement("xpath", "//a[text()='Vicky']");
	click(ele14);*/
	driver.findElementByXPath("//a[text()='Vicky']").click();
	
	switchToWindow(0);
	
	WebElement ele15 = locateElement("LinkText", "Merge");
	click(ele15);		
	
	acceptAlert();
	Thread.sleep(3000);
	WebElement ele16 = locateElement("LinkText", "Find Leads");
	click(ele16);
	WebElement ele17 = locateElement("name", "id");	
	type(ele17, "10276");
	
	WebElement ele18 = locateElement("xpath", "//button[text()='Find Leads']");
	click(ele18);
	WebElement ele19 = locateElement("xpath", "//div[text()='No records to display']");
	click(ele19);
	takeSnap();
	
	
	
	
}
}

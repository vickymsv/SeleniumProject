package testCase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import WdMethods.ProjectMethods;

public class DeleteLead extends ProjectMethods{
	@BeforeClass (groups="sanity")
	public void setData() {
		Testcasename = "TC003";
		TestCasedescription ="Delete Lead";
		Category = "regression";
		Author= "vickyjaya";
		excelfilename="deletelead";
	}
/*	@DataProvider(name="Deletelead")
	public Object[][] getData()
	{
	
	Object[][] data= new Object [2][0];
	data [0][0]="Vignesh";
	//second data
	data [1][0]="Jayaram";  
	
	return data;
	}*/
	@Test(/*dependsOnMethods="testCase.EditLead.editLead", groups="regression",*/ dataProvider="fetchdata")
	public void deleteLead(String fname) throws InterruptedException {
		
		
		WebElement ele1 =locateElement("xpath", "(//div[@class='x-panel-header']/a)[2]");
		click(ele1);
		
		WebElement ele2 =locateElement("xpath", "(//a[@href='/crmsfa/control/createLeadForm']/following::a)[1]");
		click(ele2);
		
		WebElement ele3 =locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(ele3, fname);
		
		WebElement ele4 =locateElement("xpath", "//button[contains(text(),'Find Leads')]");
		click(ele4);
		
		Thread.sleep(3000);
		
		//String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		WebElement ele5 =locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(ele5);
		
		verifyTitle("View Lead | opentaps CRM");
		
		WebElement ele6 =locateElement("xpath", "//a[@href='javascript:document.deleteLeadForm.submit()']");
		click(ele6);
		
		/*click(locateElement("linkText", "Find Leads"));
		type(locateElement("xpath", "//label[contains(text(),'Lead ID:')]/following::input"), txt);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		verifyPartialText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");		*/
	}
}


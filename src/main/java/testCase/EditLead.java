package testCase;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import WdMethods.ProjectMethods;


public class EditLead extends ProjectMethods{

	@BeforeClass (groups="sanity")
	public void setData() {
		Testcasename = "TC002";
		TestCasedescription ="Edit Lead";
		Category = "Sanity";
		Author= "vickyjaya";
		excelfilename="editlead";
	}
	
	
	/*public Object[][] getData()
	{
	
	Object[][] data= new Object [2][2];
	data [0][0]="Vignesh";
	data [0][1]="Distribution";

	//second data
	data [1][0]="Jayaram";
	data [1][1]="Finance";
	return data;
	}*/
	@Test(/*dependsOnMethods="testCase.TC001_CreateLead.createlead", groups="sanity",*/ dataProvider="fetchdata")
public void editLead(String fname, String industry) throws InterruptedException {
				
		WebElement ele1 =locateElement("xpath", "(//div[@class='x-panel-header']/a)[2]");
		click(ele1);
		
		WebElement ele2 =locateElement("xpath", "(//a[@href='/crmsfa/control/createLeadForm']/following::a)[1]");
		click(ele2);
		
		WebElement ele3 =locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(ele3, fname);
		
		WebElement ele4 =locateElement("xpath", "//button[contains(text(),'Find Leads')]");
		click(ele4);
		
		Thread.sleep(3000);
		WebElement ele5 =locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(ele5);
	
		
		WebElement ele6 =locateElement("xpath", "//a[contains(text(),'Edit')]");
		click(ele6);
		
	
		
		WebElement ele7 =locateElement("id", "updateLeadForm_industryEnumId");
		selectDropDownUsingText(ele7, industry);
		
		WebElement ele8 =locateElement("xpath", "//input[@class='smallSubmit']");	
		click(ele8);
	}

}
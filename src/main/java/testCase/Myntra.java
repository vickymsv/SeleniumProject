package testCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
public class Myntra {
	@Test
	public void myntralaunch() throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.myntra.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		WebElement leftCount = driver.findElementByXPath("(//span[@class='categories-num'])[1]");
		String text = leftCount.getText();
			System.out.println(text);	
			String replaceAll = text.replaceAll("\\D", "");
		System.out.println(replaceAll);


		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();

		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		WebElement rightCount = driver.findElementByXPath("//h1[contains(text(),'Mens Jackets')]/following::span");
		String text2 = rightCount.getText();
		String replaceAll2 = text2.replaceAll("\\D", "");
		System.out.println(replaceAll2);
		
		WebElement leftCount1 = driver.findElementByXPath("(//span[@class='categories-num'])[1]");
		String text3 = leftCount1.getText();
			System.out.println(text3);	
			String replaceAll3 = text3.replaceAll("\\D", "");
		System.out.println(replaceAll3);

		// If both count matches, say success
		if(replaceAll3.equals(replaceAll2)) {
			System.out.println("The count matches on either case");
		}else {
			System.err.println("The count does not match");
		}

		// Click on Offers
		driver.findElementByXPath("(//h4[@class='atsa-title'])[7]").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<Integer> onlyPrice = new ArrayList<Integer>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(Integer.parseInt(productPrice.getText().replaceAll("\\D", "")));
		}
		
		System.out.println(onlyPrice);

		// Sort them 
		Integer max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		Thread.sleep(5000);
		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByXPath("//div[@class='brand-more']").click();			
		driver.findElementByXPath("//input[@value='Allen Solly']/following::div").click();
		driver.findElementByXPath("//span[@class='myntraweb-sprite FilterDirectory-close sprites-remove']").click();
	
		// Find the costliest Jacket
		
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		

		List<Integer> onlyPrice1 = new ArrayList<Integer>(); 

		for (WebElement productPrice : allenSollyPrices) {
			onlyPrice1.add(Integer.parseInt(productPrice.getText().replaceAll("\\D", ""))); 
		}
		

		// Get the minimum Price 
		Integer min = Collections.min(onlyPrice1);

		// Find the lowest priced Allen Solly
		System.out.println(min);

		driver.close();
	}

}

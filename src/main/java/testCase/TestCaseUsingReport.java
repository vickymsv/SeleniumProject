package testCase;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class TestCaseUsingReport {
	public static ExtentReports extent;
	public static ExtentTest test;
	public static String Testcasename, TestCasedescription, Author, Category, excelfilename;
@BeforeSuite(groups="common") //Onetime execution 
	public void createHtml()
	{
		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);
	}
@BeforeMethod(groups="common")
	public void createTest()
	{
		ExtentTest test = extent.createTest(Testcasename, TestCasedescription);
		test.assignAuthor(Author);
		test.assignCategory(Category);
	}

public void reportStep(String desc,String status)
{
	if (status.equalsIgnoreCase("pass")) {
		test.pass(desc);
	}if (status.equalsIgnoreCase("fail")) {
		test.fail(desc);
	}
}
@AfterSuite(groups="common")
public void flush() {
	extent.flush();
}
}











package testCase;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class SampleReportExtend {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/result.html");
				html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_CreateLead", "Creating a lead");
		
test.assignAuthor("vickyjaya");
test.assignCategory("smoke test");

test.pass("The username entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
test.pass("The password entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img10.png").build());
test.fail("The login button clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img11.png").build());
extent.flush();


	}

}

package testCase;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import WdMethods.ProjectMethods;
import WdMethods.SeMethods;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeClass (groups="smoke")
	public void setData() {
		Testcasename = "TC001";
		TestCasedescription ="create lead";
		Category = "Smoke";
		Author= "vicky";
		excelfilename="createlead";
	}
		
	/*public Object[][] getData()
	{
	
	Object[][] data= new Object [2][4];
	data [0][0]="TestLeaf";
	data [0][1]="Vignesh";
	data [0][2]="Manisundaram";
	data [0][3]="Conference";
	//second data
	data [1][0]="Infosys";
	data [1][1]="Jayaram";
	data [1][2]="Manoharan";
	data [1][3]="Partner";
	return data;
	}*/
	
	@Test(/*groups="smoke",*/dataProvider="fetchdata")
	public void createlead(String cname, String fname, String lname, String Source) {
		
		WebElement ele5 = locateElement("LinkText", "Create Lead");
		click(ele5);
		
		WebElement ele6 = locateElement("id", "createLeadForm_companyName");
		type(ele6, cname);
		
		WebElement ele7 = locateElement("id", "createLeadForm_firstName");
		type(ele7, fname);
		
		WebElement ele8 = locateElement("id", "createLeadForm_lastName");
		type(ele8, lname);
		
				
		WebElement ele10 = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(ele10, Source);
		
		WebElement ele9 = locateElement("class", "smallSubmit");
		click(ele9);
		
		WebElement ele11 = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(ele11, "VickyJaya");
		
	
		
	}
	

}

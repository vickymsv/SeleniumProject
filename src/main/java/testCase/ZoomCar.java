package testCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import WdMethods.ProjectMethods;
import WdMethods.SeMethods;

public class ZoomCar extends SeMethods {

	/*	@BeforeMethod
	public void setData() {
		Testcasename = "BookACar";
		TestCasedescription ="Car booking";
		Author= "vickyjaya";
		Category = "Sanity";
	}*/

	@Test
	public void car() throws InterruptedException
	{
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement elez1 = locateElement("class","search");
		click(elez1);
		WebElement elez2 = locateElement("xpath","//div[contains(text(),'Thuraipakkam')]");
		click(elez2);
		WebElement elez3 = locateElement("class","proceed");
		click(elez3);

		WebElement elez4 = locateElement("xpath","//div[@class='text']/following::div");
		Thread.sleep(3000);
		String text1= elez4.getText();
		System.out.println(text1);
		click(elez4);


		WebElement elez5 = locateElement("class","proceed");
		click(elez5);
		Thread.sleep(3000);
		WebElement elez6 = locateElement("xpath","//div[@class='day picked ']");
		Thread.sleep(3000);
		String text2 = elez6.getText();
		System.out.println(text2);

		verifyPartialText(elez6, text1);

		WebElement elez7 = locateElement("class","proceed");
		click(elez7);
		Thread.sleep(3000);

		List<WebElement> results = driver.findElementsByXPath("//div[@class='car-listing' or @class='suggested-item']");

		System.out.println(results.size());
		
		
		
		/*//using the application sort method.
		WebElement elez8 = locateElement("xpath","//div[@class='sort-bar car-sort-layout']//div");
		click(elez8);
		Thread.sleep(3000);
		WebElement elez9 = locateElement("xpath","(//div[@class='details']/h3)[1]");
		System.out.println(elez9.getText());
		
		WebElement elez10 = locateElement("xpath","(//button[@class='book-car'])[1]");
		click(elez10);*/
		
		//Using collections
		List<WebElement> carprice = driver.findElementsByXPath("//div[@class='price']");
		Set<Integer> pricedCar = new TreeSet<Integer>();


		for (WebElement eachCar : carprice) {
			System.out.println(eachCar.getText().replaceAll("\\D", ""));
			pricedCar.add(Integer.parseInt(eachCar.getText().replaceAll("\\D", "")));
		}

		List<Integer> priceCar = new ArrayList<Integer>(); 
		priceCar.addAll(pricedCar);
		int maxPrice = priceCar.get(priceCar.size()-1);

		System.out.println(maxPrice);

		String text = driver.findElementByXPath("(//div[contains(text(),'"+maxPrice+"')]/preceding::div[@class='car-name'])[last()]").getText();
		System.out.println(text);

		driver.findElementByXPath("(//div[contains(text(),'"+maxPrice+"')]/following::button)[1]").click();
		
	}

}

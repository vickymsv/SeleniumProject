package testCase;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import WdMethods.SeMethods;

public class FaceBook extends SeMethods {

	
	@Test
	public void LikeTestLeaf()
	{
			
		startApp("chrome", "https://www.facebook.com/");

		WebElement elef1 = locateElement("id","email");
		type(elef1, "vickymsv1211@gmail.com");

		WebElement elef2 = locateElement("id","pass");
		type(elef2, "manisundaram");

		WebElement elef3 = locateElement("xpath","//input[@type='submit']");
		click(elef3);
		
		WebElement elef4 = locateElement("xpath","//input[@data-testid='search_input']");
		type(elef4, "TestLeaf");
		
		WebElement elef5 = locateElement("xpath","//button[@data-testid='facebar_search_button']");
		click(elef5);

		WebElement elef6 = locateElement("xpath","//div[@class='_52eh _5bcu']");

		/*String text = elef6.getText();
		System.out.println(text);*/

		verifyExactText(elef6, "TestLeaf");
		WebElement elef7 = locateElement("xpath","(//div[@class='_52eh _5bcu'])[1]");
		click(elef7);
		
		WebElement elef8 = locateElement("xpath","//a[@data-testid='page_profile_like_button_test_id' or @data-testid='page_profile_liked_button_test_id']");
		String text = elef8.getText();
		System.out.println(text);

		if(elef8.getText().equalsIgnoreCase("liked")){			
			System.out.println("already liked TestLeaf page");
		}
		else if(elef8.getText().equalsIgnoreCase("like")){
			elef8.click();
		}
		
		WebElement elef10 = locateElement("LinkText","TestLeaf");
		click(elef10);
		
		String text2 = elef10.getText();
		if(elef10.getText().equalsIgnoreCase("Testleaf")) {
			System.out.println("The title contains" + text2);	
		}
		
		WebElement elef9 = locateElement("xpath","//div[contains(text(),'people like this')]");
		String text1 = elef9.getText();
		System.out.println(text1);
		
			}
}

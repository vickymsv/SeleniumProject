package testCase;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import WdMethods.SeMethods;

public class JiraGoal extends SeMethods{
	@BeforeClass (groups="smoke")
	public void setData() {
		Testcasename = "TC001";
		TestCasedescription ="create lead";
		Category = "Smoke";
		Author= "vicky";
		excelfilename="createlead";
	}
	@Test
	public void dashboard() throws InterruptedException
	{
		startApp("chrome", "https://jira.perficient.com/secure/Dashboard.jspa");
		
		WebElement elef1 = locateElement("id","login-form-username");
		type(elef1, "vignesh.manisundaram");
		
		WebElement elef2 = locateElement("id","login-form-password");
		type(elef2, "Ayodhiyapattinam@321");
		
		WebElement elef3 = locateElement("xpath","//input[@id='login']");
		click(elef3);
		
		Thread.sleep(5000);
		WebElement elef4 = locateElement("xpath","//*[@id=\"home_link\"]");
		click(elef4);
		
		WebElement elef5 = locateElement("LinkText","Manage Dashboards");
		click(elef5);
		
		WebElement elef6 = locateElement("LinkText","Create new dashboard");
		click(elef6);
		
		WebElement elef7= locateElement("xpath","//span[@class='aui-icon icon-required']/following::input");
		type(elef7, "MyHome");
		
		WebElement elef8= locateElement("xpath","//div[@class='field-group']/textarea");
		type(elef8, "MyHome");
		
		WebElement elef9 = locateElement("id","add-dashboard-submit");
		click(elef9);
		
		WebElement elef10 = locateElement("LinkText","MyHome");
		click(elef10);
		
		WebElement elef11= locateElement("xpath","//a[@class='add-gadget-link']");
		click(elef11);

		WebElement elef12 = locateElement("id","load-more-directory-items");
		click(elef12);
		
		WebElement elef16= locateElement("xpath","(//button[@class='aui-button add-dashboard-item-selector'])[4]");
		click(elef16);
		Thread.sleep(3000);
		WebElement elef17= locateElement("xpath","(//button[@class='aui-button add-dashboard-item-selector'])[2]");
		click(elef17);
		Thread.sleep(3000);
		WebElement elef18= locateElement("xpath","(//button[@class='aui-button add-dashboard-item-selector'])[26]");
		click(elef18);
		Thread.sleep(3000);
		WebElement elef19= locateElement("xpath","(//button[@class='aui-button add-dashboard-item-selector'])[1]");
		click(elef19);
		Thread.sleep(3000);
		WebElement elef20= locateElement("xpath","(//button[@class='aui-button add-dashboard-item-selector'])[17]");
		click(elef20);
		Thread.sleep(3000);
		WebElement elef14 = locateElement("xpath","//span[@class='aui-icon aui-icon-small aui-iconfont-close-dialog']");
		click(elef14);		
		Thread.sleep(3000);
		
		WebElement elef31 = locateElement("xpath","(//iframe[@class='gadget-iframe'])[2]");	
		
		driver.switchTo().frame(elef31);
		
		WebElement elef13= locateElement("xpath","//button[@class='submit button']");
		click(elef13);
		driver.switchTo().defaultContent();
		
        WebElement elef32 = locateElement("xpath","(//iframe[@class='gadget-iframe'])[1]");	
		
		driver.switchTo().frame(elef32);	
		WebElement elef15= locateElement("xpath","//input[@class='button save']");
		click(elef15);
		driver.switchTo().defaultContent();
		WebElement elef22= locateElement("xpath","(//input[@class='button submit'])");
		click(elef22);
		
		 WebElement drag = driver.findElementByXPath("(//h3[@class='dashboard-item-title'])[5]");
		 WebElement drop = driver.findElementByXPath("(//a[text()='add a new gadget.'])[2]");
		 
		 Actions builder = new Actions(driver);
		 builder.dragAndDrop(drag, drop).perform();
		 
		 
		
		 WebElement drag1 = driver.findElementByXPath("//h3[text()='Activity Stream']");
		
		 builder.dragAndDrop(drag1, drag).perform();
		
		 WebElement drag2 = driver.findElementByXPath("//h3[text()='Quick Links']");
		
		 builder.dragAndDrop(drag2, drag).perform();
		
		 WebElement drag3 = driver.findElementByXPath("//h3[text()='Agile Wallboard Gadget']");
		
		 builder.dragAndDrop(drag3, drag).perform();
		
		 WebElement drag4 = driver.findElementByXPath("//h3[text()='Assigned to Me']");
	
		 builder.dragAndDrop(drag4, drag).perform();

		 		
	}
	
}

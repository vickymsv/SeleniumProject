package testCase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import WdMethods.SeMethods;

public class ZohoForms extends SeMethods{
	@BeforeClass (groups="smoke")
	public void setData() {
		Testcasename = "Zoho1";
		TestCasedescription ="Zohoforms";
		Category = "Smoke";
		Author= "vicky";
		excelfilename="createlead";
	}
	@Test
	public void dashboard() throws InterruptedException
	{
		startApp("chrome", "https://www.zoho.com/forms/");
		WebElement elef1 = locateElement("LinkText","LOGIN");
		click(elef1);
	}
}

package seleniumHomeWork;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://Leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Manisundaram");
		driver.findElementByXPath(" //button[text()='Find Leads']").click();

		List<WebElement> count = driver.findElementsByLinkText("Manisundaram");
		count.get(1).click();
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Wifin technologies");
		driver.findElementByClassName("smallSubmit").click();
		
		driver.findElementByXPath("//span[text()='Wifin technologies (10350)']");

		System.out.println("Lead Updated");
		driver.close();
	}

}

package seleniumHomeWork;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("http://Leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByLinkText("Email").click();
		driver.findElementByName("emailAddress").sendKeys("vickymsv1211@gmail.com");
		driver.findElementByXPath(" //button[text()='Find Leads']").click();
		driver.findElementByXPath("//a[text()='Vignesh']").click();
		driver.findElementByLinkText("Duplicate Lead").click();
		driver.findElementByXPath("//div[contains(text(),'Duplicate Lead')]");
		
		System.out.println("The Title is Duplicate Lead- Confirmed");
		
		driver.findElementById("createLeadForm_firstName").clear();
		driver.findElementById("createLeadForm_firstName").sendKeys("Vickysundaram");
		driver.findElementByClassName("smallSubmit").click();
		driver.findElementByXPath("//span[contains(text(),'Vickysundaram')]");
		System.out.println("Duplicate Lead created");
		driver.close();
	}

}

package seleniumHomeWork;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("http://Leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@alt='Lookup'])[1]").click();
		//moving to second window
		Set<String> Allwindows = driver.getWindowHandles();
		System.out.println(Allwindows.size());

		List<String> listOfWindows= new ArrayList<String>();
		listOfWindows.addAll(Allwindows);
		//moving to second window
		driver.switchTo().window(listOfWindows.get(1));
		
		driver.findElementByName("firstName").sendKeys("vignesh");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		/*String text =driver.findElementByXPath("//div[@unselectable='on']/a").getText();
		System.out.println(text);*/
		driver.findElementByXPath("//a[text()='Vignesh1']").click();
		
		//moving to first window
		driver.switchTo().window(listOfWindows.get(0));
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		//moving to second window again
		
		Set<String> Allwindows1 = driver.getWindowHandles();
		System.out.println(Allwindows1.size());

		List<String> listOfWindows1= new ArrayList<String>();
		listOfWindows1.addAll(Allwindows1);
		
		//moving to second window
		driver.switchTo().window(listOfWindows1.get(1));
	
		driver.findElementByName("firstName").sendKeys("vicky");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//a[text()='Vicky']").click();
		//moving to first window again
		driver.switchTo().window(listOfWindows1.get(0));
		
		driver.findElementByLinkText("Merge").click();
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		//String text =driver.findElementByXPath("(//span[@class='tabletext'])[3]").getText();

        driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys("10144");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		driver.findElementByXPath("//div[text()='No records to display']");
		
		File source = driver.getScreenshotAs(OutputType.FILE);
		
		File destinataion =new File("./Screenshots/img.png");
		
		FileUtils.copyFile(source, destinataion);
	
	}

}

package seleniumHomeWork;



import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		
		WebElement drp1 = driver.findElementById("dropdown1");
		Select sel1= new Select(drp1);
		List<WebElement> count = sel1.getOptions();
		sel1.selectByIndex(count.size()-1);
		
		WebElement drp2 = driver.findElementByName("dropdown2");
		Select sel2= new Select(drp2);
		List<WebElement> count1 = sel2.getOptions();
		sel2.selectByIndex(count1.size()-1);
		
		WebElement drp3 = driver.findElementById("dropdown3");
		Select sel3= new Select(drp3);
		List<WebElement> count2 = sel3.getOptions();
		sel3.selectByIndex(count2.size()-1);
		
		WebElement drp4 = driver.findElementByClassName("dropdown");
		Select sel4= new Select(drp4);
		List<WebElement> count3 = sel4.getOptions();
		sel4.selectByIndex(count3.size()-1);
		
		WebElement drp5 = driver.findElementByXPath(" (//div[@class='example'])[5]/select");
		Select sel5= new Select(drp5);
		List<WebElement> count4 = sel5.getOptions();
		sel5.selectByIndex(count4.size()-1);
		
		WebElement drp6 = driver.findElementByXPath("(//div[@class='example'])[5]/following::select");
		Select sel6= new Select(drp6);
		List<WebElement> count5 = sel6.getOptions();
		sel6.selectByIndex(count5.size()-1);
		
	}

}

package WdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import testCase.ReadExcel;

public class ProjectMethods extends SeMethods{
	
@DataProvider(name="fetchdata")	
	
	public Object[][] getData() throws IOException {
	Object[][] excelData=ReadExcel.readExcel(excelfilename);
	return excelData;
	}

	@BeforeMethod(groups="common")
	

	@Parameters({"browser","username","appUrl", "password" })
	public void login(String browser,String username, String appUrl,String password) {
		startApp(browser, appUrl);
		WebElement ele1 = locateElement("id", "username");	
		type(ele1, username);
		WebElement ele2 = locateElement("id", "password");
		type(ele2, password);
		WebElement ele3 = locateElement("class", "decorativeSubmit");
		click(ele3);
		
		WebElement ele4 = locateElement("LinkText", "CRM/SFA");
		click(ele4);
	}
@AfterMethod(groups="common")

public void close()
{
	closeAllBrowsers();
}
}

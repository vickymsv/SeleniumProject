package Week4Day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnDragAndDrop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("https://jqueryui.com/");

		driver.manage().window().maximize();
		
		driver.findElementByLinkText("Draggable").click();
		
		driver.switchTo().frame(0);
		 WebElement drag = driver.findElementByXPath("//div[@id='draggable']/p");
		 Actions builder = new Actions(driver);
		 builder.dragAndDropBy(drag, 200,200 ).perform();
		
	}

}

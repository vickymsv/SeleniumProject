package Week4Day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertAndFrame {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		String text2 = driver.switchTo().alert().getText();
		System.out.println(text2);
		driver.switchTo().alert().sendKeys("vignesh");
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		System.out.println(text);
		driver.switchTo().defaultContent();
		driver.findElementById("tryhome").click();
	}

}

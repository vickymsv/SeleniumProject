package Week4Day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindows {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();

		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();

		Set<String> Allwindows = driver.getWindowHandles();
		System.out.println(Allwindows.size());

		List<String> listOfWindows= new ArrayList<String>();
		listOfWindows.addAll(Allwindows);

		String secondwindow = listOfWindows.get(1);

		driver.switchTo().window(secondwindow);

		String title = driver.getTitle();

		System.out.println(title);

		String currentUrl = driver.getCurrentUrl();

		System.out.println(currentUrl);

		driver.quit();

	}

}

package Week3Day1;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Leaftap {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("http://Leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementById("createLeadForm_companyName").sendKeys("Perficient");
		driver.findElementById("createLeadForm_firstName").sendKeys("Vicky");
		driver.findElementById("createLeadForm_lastName").sendKeys("Manisundaram");
		
		
		//dropdown
		WebElement drp = driver.findElementById("createLeadForm_dataSourceId");
		Select sel= new Select(drp);
	sel.selectByVisibleText("Conference");
	
	WebElement drp1 = driver.findElementById("createLeadForm_marketingCampaignId");
	Select sel1= new Select(drp1);
	sel1.selectByValue("CATRQ_CARNDRIVER");
	
	driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Vignesh");
	driver.findElementById("createLeadForm_lastNameLocal").sendKeys("M");
	driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr");
	driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Test Engineer");
	driver.findElementById("createLeadForm_departmentName").sendKeys("Testing");
	driver.findElementById("createLeadForm_annualRevenue").sendKeys("100000");
	
	WebElement drp2 = driver.findElementById("createLeadForm_currencyUomId");
	Select sel2= new Select(drp2);
	sel2.selectByVisibleText("ADP - Andoran peseta");
	
	WebElement drp3 = driver.findElementById("createLeadForm_industryEnumId");
	Select sel3= new Select(drp3);
	sel3.selectByVisibleText("Aerospace");
	
	driver.findElementById("createLeadForm_numberEmployees").sendKeys("75");
	
	WebElement drp4 = driver.findElementById("createLeadForm_ownershipEnumId");
	Select sel4= new Select(drp4);
	sel4.selectByVisibleText("Sole Proprietorship");
	
	driver.findElementById("createLeadForm_sicCode").sendKeys("9847");
	driver.findElementById("createLeadForm_tickerSymbol").sendKeys("tickmark");
	driver.findElementById("createLeadForm_description").sendKeys("Paragraph");
	driver.findElementById("createLeadForm_importantNote").sendKeys("its very important");
	
	driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("+91");
	driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("636");
	driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("91");
	driver.findElementById("createLeadForm_primaryEmail").sendKeys("vickymsv1211@gmail.com");
	
	driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("+919597452790");
	driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("anything");
	driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");

	
	driver.findElementById("createLeadForm_generalToName").sendKeys("sundaram");
	driver.findElementById("createLeadForm_generalAttnName").sendKeys("Mani");
	driver.findElementById("createLeadForm_generalAddress1").sendKeys("Earikadu");
	driver.findElementById("createLeadForm_generalAddress2").sendKeys("Ayodhiyapattinam");
	driver.findElementById("createLeadForm_generalCity").sendKeys("salem");
	driver.findElementById("createLeadForm_generalPostalCode").sendKeys("636103");
	driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("636");
	
	
	
	
	WebElement drp5 = driver.findElementById("createLeadForm_generalCountryGeoId");
	Select sel5= new Select(drp5);
	sel5.selectByVisibleText("India");
	
	Thread.sleep(3000);
	WebElement drp6 = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
	Select sel6= new Select(drp6);
	sel6.selectByVisibleText("TAMILNADU");
	
	driver.findElementByName("submitButton").click();
	
	File source = driver.getScreenshotAs(OutputType.FILE);
	
	File destinataion =new File("./Screenshots/img.png");
	
	FileUtils.copyFile(source, destinataion);
	
	
	/*List<WebElement> printall = sel1.getOptions();
	
	for (WebElement eachprint : printall) {
		
		System.out.println(eachprint.getText());	
	}
			System.out.println(printall.size());*/
		
		}

}
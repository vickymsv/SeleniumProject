package Week1Day1;

public class bank {

	static boolean isActive ;
	static String accountType ="savings";
	static int accountNumber = 7842239 ;
	static float rateOfInterest ;
	static int minBalance ;
	static String accountHolderFirstName;
	static char accountHolderInitial;

	public static void createAccount()
	{
		accountType= "Savings";
		accountNumber= 123456;
		isActive= true;
		rateOfInterest= 10.5f;
		minBalance = 500;
		accountHolderFirstName ="BABU";
		accountHolderInitial= 'M';
		System.out.println("Account created successfully");
	}
	public static boolean blockaccount(int accnumber)
	{
		isActive= false;
		System.out.println(accnumber + " Account blocked");
		return isActive;
	}
	public static void printaccountdetails()
	{
		System.out.println("Account type "+ accountType );
		System.out.println("Account number "+ accountNumber );
	}
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		createAccount();
		boolean Accstatus= blockaccount(94637);
		System.out.println("Account status " + Accstatus);
		printaccountdetails();
	}

}

package Week1Day1;

public class Operators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n1=12;
		int n2=13;

		//arithmetic operator
		System.out.println(n1+n2);
		System.out.println(n1-n2);
		System.out.println(n1*n2);
		System.out.println(n1%n2);
		System.out.println(n1/n2);

		// Relational operator
		System.out.println(n1==n2);
		System.out.println(n1!=n2);
		System.out.println(n1>n2);
		System.out.println(n1<n2);
		System.out.println(n1>=n2);
		System.out.println(n1<=n2);
	}

}

package homeworkanddailytask;

import java.util.Scanner;

public class MultiplyUptoGivenN {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in); 
		System.out.println("Enter the table:");
		int table = scan.nextInt();  
		
		Scanner scan1 = new Scanner(System.in); 
		System.out.println("Enter the number up to which:");
		int n = scan1.nextInt();  
		
		for( int i=1; i<=n; i++)
		{
			int multiple=i*table;
			System.out.println(i +"*"+n+"="+ multiple);
		}
		
	}

}

package homeworkanddailytask;

import java.util.Scanner;

public class SwapWithOutAVariable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scan = new Scanner(System.in); 
		System.out.println("Enter the value of a:");
		int a=scan.nextInt();
		
		Scanner scan1 = new Scanner(System.in); 
		System.out.println("Enter the value of b:");
		int b=scan1.nextInt();
		
		  a = a+b;
	        b=a-b;
	        a=a-b;
		
	        System.out.println("a value: "+a);
	        System.out.println("b value: "+b);
	}

}

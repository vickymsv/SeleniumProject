package hwweek2;

import java.util.ArrayList;
import java.util.List;

public class DuplicateNumberInGivenArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a[] = { 13, 65, 15, 67, 88, 65, 13, 99, 67, 13, 65, 87, 13 };

		List<Integer> values = new ArrayList<Integer>();
		List<Integer> adding = new ArrayList<Integer>();
		List<Integer> duplicate = new ArrayList<Integer>();
		for (int i = 0; i < a.length; i++) {
			values.add(a[i]);
		}
		for (Integer c : values) {
			if (adding.contains(c)&&!duplicate.contains(c)) {
				duplicate.add(c);  
			} else {
				adding.add(c);
			}
		}
		
		for (Integer c : duplicate) {
			System.out.println(c);
		}

	}
}

package hwweek2;

import java.util.LinkedHashMap;
import java.util.Map;

public class PrintNumberOfcharacter {
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s = "amazon india development center";


		Map<Character, Integer> charn = new LinkedHashMap<Character, Integer>();

		s=s.replace(" ", "");

		for (char c: s.toCharArray())
		{
			if(charn.containsKey(c))
			{
				charn.put(c,charn.get(c)+ 1);
			}
			else 
			{
				charn.put(c, 1);
			}

		}	

		System.out.println(charn);
	}

}

package week2day2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class mapclass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<String, Integer> tvnames = new LinkedHashMap<String, Integer>();

		tvnames.put("LG", 2);
		tvnames.put("Samsung", 3);
		tvnames.put("Onida", 1);
		tvnames.put("Sony", 2);
		tvnames.put("Panasonic", 3);
		tvnames.put("Micromax", 2);
		//print sum of all tv
		int sum = 0;

		for(Entry<String, Integer> mob :tvnames.entrySet())
		{
			sum = sum + mob.getValue();	

		}
		System.out.println(sum);


		Set<String> allTvs = tvnames.keySet();
		List<String> allTeleVisions = new ArrayList<String>();
		allTeleVisions.addAll(allTvs);

		System.out.println(allTeleVisions.get(allTeleVisions.size()-1));

		Integer countOfLastTv = tvnames.get(allTeleVisions.get(allTeleVisions.size()-1));
		System.out.println(countOfLastTv);

		tvnames.put(allTeleVisions.get(allTeleVisions.size()-1),countOfLastTv-1);
		System.out.println(tvnames);

	}

}

